from django.shortcuts import render
from django.http import HttpResponse

from .tasks import sleepy, send_email_task

# def index(request):
#     sleepy.delay(10)
#     return HttpResponse('<h1>TASK IS DONE!</h1>')

def index(request):
    send_email_task.delay()
    return HttpResponse('<h1>EMAIL HAS BEEN SENT WITH CELERY!</h1>')